<?php

use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;

class MoodleClient implements \Client
{

	private $editorId;
	private $cookie;
	private $sesskey;
	private $editorItemId;
	private $options = [];
	private $guzzleClient;

	const USERNAME = 'petra.schaer';
	const PASSWORD = 'hzoritra31';
	const BASEURI = 'http://moodle.bbbaden.ch';
	const LOGINURI = '/login/index.php';
	const EDITURI = '/mod/journal/edit.php';
	const SERVICE = 'moodle_mobile_app';

	/**
	 * MoodleHelper constructor.
	 *
	 * @param $editorId
	 */
	public function __construct($editorId)
	{
		$this->editorId = $editorId;
		$this->guzzleClient = new Client([
			'base_uri' => MoodleClient::BASEURI,
			'verify' => false,
			'cookies' => true,
		]);
	}

	public function getLogContents()
	{
		$response = $this->guzzleClient->request(
			'GET',
            MoodleClient::EDITURI . '?id=' . $this->editorId,
			['cookies' => $this->cookie]
		);
		$content = $response->getBody()->getContents();
		$start = strpos($content, '<div id="id_text_editoreditable" contenteditable="true" role="textbox" spellcheck="true" aria-live="off" class="editor_atto_content " aria-labelledby="yui_3_17_2_1_1570387442045_314" style="min-height: 308px; height: 308px;">');
		$end = strpos($content, '</div>', $start);
		$content = substr($content, $start, $end - $start);
		die($content);
	}

	public function postLogContents($content)
	{
		$response = $this->guzzleClient->post(
			'https://moodle.bbbaden.ch/mod/journal/edit.php',
			[
			    'headers' => [
                    'Connection' => 'keep-alive',
                    'Cache-Control' => 'max-age=0',
                    'Origin' => 'https://moodle.bbbaden.ch',
                    'Upgrade-Insecure-Requests' => '1',
                    'Content-Type' => 'application/x-www-form-urlencoded',
                    'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36',
                    'Sec-Fetch-Mode' => 'navigate',
                    'Sec-Fetch-User' => '?1',
                    'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
                    'Sec-Fetch-Site' => 'same-origin',
                    'Referer' => 'https://moodle.bbbaden.ch/mod/journal/edit.php?id=' . strval($this->editorId),
                    'Accept-Encoding' => 'gzip, deflate, br',
                    'Accept-Language' => 'de-DE,de;q=0.9,en-US;q=0.8,en;q=0.7',
				    'Cookie' => 'MoodleSession=vmm6ah5iaatb5kajmb2k5ig0c5',
                ],
				'form_params' => [
                    'id' => $this->editorId,
                    'sesskey' => $this->sesskey,
                    '_qf__mod_journal_entry_form' => 1,
                    'text_editor[text]' => $content,
                    'text_editor[format]' => 1,
                    'text_editor[itemid]' => $this->editorItemId,
                    'submitbutton' => 'Änderungen speichern',
				],
			]
		);
	}

	public function retreiveCookie()
	{
		$response = $this->guzzleClient->request('POST', 'https://moodle.bbbaden.ch/login/index.php', [
			'form_params' => [
				'username' => MoodleClient::USERNAME,
				'password' => MoodleClient::PASSWORD,
			],
		]);

		$this->cookie = $this->guzzleClient->getConfig('cookies');
	}

	public function retreiveSesskey()
	{
		$response = $this->editRequest();

		$crawler = new Crawler($response->getBody()->getContents());
		$crawler = $crawler->filter('input')->reduce(function (Crawler $node, $i) {
			if ($node->attr('name') === 'sesskey') {
				return true;
			} else {
				return false;
			}
		});
		$this->sesskey = $crawler->attr('value');
	}

    public function retreiveEditorItemId()
    {
        $response = $this->editRequest();

        $crawler = new Crawler($response->getBody()->getContents());
        $crawler = $crawler->filter('input')->reduce(function (Crawler $node, $i) {
            if ($node->attr('name') === 'text_editor[itemid]') {
                return true;
            } else {
                return false;
            }
        });
        $this->editorItemId = $crawler->attr('value');
    }

    private function editRequest() {
        return $this->guzzleClient->request(
            'GET',
            MoodleClient::EDITURI . '?id=' . $this->editorId,
            ['cookies' => $this->cookie]
        );
    }

}
