<?php
require 'contracts/Client.php';

class ClientProxy
{

	const ACTION_GET_LOG_CONTENTS = 0;
	const ACTION_POST_LOG_CONTENTS = 1;
	const ACTION_GET_AUTH_COOKIE = 2;
	const ACTION_GET_FORM_SESSKEY = 3;

	/**
	 * @var \Client $client
	 */
	private $client;

	/**
	 * ClientProxy constructor.
	 *
	 * @param \Client $client
	 */
	public function __construct(\Client $client)
	{
		$this->client = $client;
	}

	/**
	 * Invoke an action on client
	 *
	 * @param $action int action id (use ClientProxy action constants)
	 * @param $options array options containing parameters for the methods on the client
	 */
	public function invoke($action, $options = [])
	{
		$this->client->retreiveCookie();
		switch ($action) {
			case ClientProxy::ACTION_GET_LOG_CONTENTS:
				$this->client->getLogContents();
				break;
			case ClientProxy::ACTION_POST_LOG_CONTENTS:
				$this->client->retreiveSesskey();
				$this->client->retreiveEditorItemId();
				$this->client->postLogContents($options['text']);
				break;
            case ClientProxy::ACTION_GET_AUTH_COOKIE:
                break;
            case ClientProxy::ACTION_GET_FORM_SESSKEY:
                $this->client->retreiveSesskey();
                break;
		}
	}

}
