<?php

interface Client
{

	public function getLogContents();
	public function postLogContents($content);

	public function retreiveCookie();
	public function retreiveSesskey();
    public function retreiveEditorItemId();

}
