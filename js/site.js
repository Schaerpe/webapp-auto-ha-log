$(document).ready(colors());


function colors() {
	$('#myCarousel').on('slide.bs.carousel', function () {
		var div = $('div.active');
		var img = div.children()[0];

		var vibrant = new Vibrant(img);
		var swatches = vibrant.swatches();
		var colorPrimary = swatches.Vibrant.getHex();
		var colorSecondary = swatches.DarkVibrant.getHex();
		$('footer').css('backgroundColor', colorPrimary);
		$('#main-btn').css({
			'background-color': colorPrimary,
			'border-color': colorSecondary
		});
	});
}



