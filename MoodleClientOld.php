<?php

class MoodleClientOld implements Client //Todo: Clean this shit up
{

	private $editorId;
	private $cookie;
	private $sesskey;
	private $request;

	const USERNAME = 'petra.schaer';
	const PWHASH = '74465844B6956B3939A404177EE3D53A';
	const LOGINURL = 'https://moodle.bbbaden.ch/login/';
	const EDITURL = 'https://moodle.bbbaden.ch/mod/journal/edit.php';
	const SERVICE = 'moodle_mobile_app';

	/**
	 * MoodleHelper constructor.
	 *
	 * @param $editorId
	 */
	public function __construct($editorId)
	{
		$this->editorId = $editorId;
	}

	public function getLogContents()
	{
		//Request: See Postman get log contents request
		//Get HTML content of div with class "box generalbox" and remove first child (div) which is button
	}

	public function postLogContents($content)
	{

	}

	public function retreiveCookie()
	{
		$this->initRequest(-1);
		$response = curl_exec($this->request);
		if ($response !== false && empty(curl_error($this->request))) {
			//Retrieve cookie
			preg_match('/^Set-Cookie:\s*([^;]*);/mi', $response, $m);
			$this->cookie = $m[1];
		}
		$this->close();
	}

	public function initRequest($action)
	{
		ini_set("xdebug.var_display_max_children", -1);
		ini_set("xdebug.var_display_max_data", -1);
		ini_set("xdebug.var_display_max_depth", -1);

		$this->request = curl_init();

		$options = [

		];

		switch ($action) {
			case ClientProxy::ACTION_GET_LOG_CONTENTS:
				$options = array_merge($options, [
					CURLOPT_URL => ''
				]);
				break;
			case ClientProxy::ACTION_POST_LOG_CONTENTS:
				$options = [
					CURLOPT_SSL_VERIFYHOST => 0,
					CURLOPT_SSL_VERIFYPEER => 0,
					CURLOPT_RETURNTRANSFER => 1,
					CURLOPT_HEADER => 1,
					CURLOPT_URL => MoodleClientOld::EDITURL . '?id=' . $this->editorId,
					CURLOPT_POST => 1,
					CURLOPT_HTTPHEADER => [
						'Cookie' => $this->cookie,
					],
				];
				break;
			default:
				//default case is login
				$options = [
					CURLOPT_SSL_VERIFYHOST => 0,
					CURLOPT_SSL_VERIFYPEER => 0,
					CURLOPT_RETURNTRANSFER => 1,
					CURLOPT_HEADER => 1,
					CURLOPT_URL => MoodleClientOld::LOGINURL,
					CURLOPT_POST => 1,
					CURLOPT_POSTFIELDS => [
						'username' => MoodleClientOld::USERNAME,
						'password' => MoodleClientOld::PWHASH,
						'rememberusername' => 0,
						'service' => MoodleClientOld::SERVICE
					]
				];
				break;
		}

		curl_setopt_array($this->request, $options);
	}

	public function close()
	{
		curl_close($this->request);
	}

	public function retreiveSesskey()
	{
		$this->initRequest(ClientProxy::ACTION_POST_LOG_CONTENTS);
		$response = curl_exec($this->request);
		$pos = strpos($response, '<input name="sesskey" type="hidden" value="');
		if ($pos !== false) {
			$this->sesskey = substr($response, $pos + 1, 10);
		}

	}
}
