<?php

ini_set("xdebug.var_display_max_children", -1);
ini_set("xdebug.var_display_max_data", -1);
ini_set("xdebug.var_display_max_depth", -1);

$loginRequest = curl_init();
$haLogRequest = curl_init();

$username = 'petra.schaer';
$passwordHash = '74465844B6956B3939A404177EE3D53A';
$loginUrl = 'https://moodle.bbbaden.ch/login/';
$textExitorId = 'id_text_editoreditable';
$formId = 72480;
$sesskey = 'U32CDvZy70';
$haLogUrl = 'https://moodle.bbbaden.ch/mod/journal/edit.php?id=' . $formId;
$haLogPostFields = http_build_query([
    'form-data' => [
        'id' => $formId,
        'sesskey' => $sesskey,
        '_qf__mod_journal_entry_form' => 1,
        'text_editor[text]' => '<p>Auto generated</p>',
        'text_editor[format]' => 1,
        ]
    ]);

//Login
curl_setopt_array($loginRequest, [
    CURLOPT_URL => $loginUrl,
    CURLOPT_POST => 1,
    CURLOPT_POSTFIELDS => http_build_query([
        'username' => $username,
        'password' => $passwordHash,
        'rememberusername' => 0,
    ]),
]);

$response = curl_exec($loginRequest);
if ($response !== false && empty(curl_error($loginRequest))) {
    //Retrieve cookie
    preg_match('/^Set-Cookie:\s*([^;]*);/mi', $response, $m);
	$cookie = $m[1];

    //Ha log post request
    curl_setopt_array($haLogRequest, [
	    CURLOPT_SSL_VERIFYHOST => 0,
	    CURLOPT_SSL_VERIFYPEER => 0,
        CURLOPT_URL => $haLogUrl,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_HEADER => 1,
        CURLOPT_POST => 1,
        CURLOPT_POSTFIELDS => $haLogPostFields,

    ]);

    $response = curl_exec($haLogRequest);
    if ($response !== false && empty(curl_error($haLogRequest))) {
        echo 'success';
    } else {
        echo 'Error: "' . curl_error($haLogRequest) . '" - Code: ' . curl_errno($haLogRequest);
        die();
    }
} else {
    echo 'Error: "' . curl_error($loginRequest) . '" - Code: ' . curl_errno($loginRequest);
    die();
}

curl_close($haLogRequest);
curl_close($loginRequest);

//Load old content todo: Get request for site and load that in loadHTML()
$document = new DOMDocument();
$document->loadHTML($response);
$textEditor = $document->getElementById($textExitorId);
$nodesOld = $textEditor->childNodes;

$nodes = new DOMNodeList();



