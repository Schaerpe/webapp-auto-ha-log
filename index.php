<?php
require 'ClientProxy.php';
require 'MoodleClient.php';
require 'vendor/autoload.php';

$errorSummary = '';

if (!empty($_POST)) {
	try {
		$proxy = new ClientProxy(new MoodleClient($_POST['editorId']));
		$proxy->invoke(ClientProxy::ACTION_POST_LOG_CONTENTS, [
			'text' => sprintf('Ich habe den Auftrag %s gemacht. Dabei habe ich %s gelernt. Probleme tauchten auf bei %s.', $_POST['exercise'], $_POST['exercise'], $_POST['exercise'])]);
	} catch (Exception $e) {
		$errorSummary = $errorSummary . $e->getMessage() . PHP_EOL;
	}
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Auto HA-Log</title>
	<?php include('partials/headRelations.html') ?>
</head>
<body>
	<h1>Auto HA-Log</h1>
	<header>
		<?php include('partials/header.php') ?>
	</header>
	<main class="container-fluid">
		<h2>Hausaufgaben-Log ausfüllen</h2>
		<?php include('partials/form.php') ?>
	</main>
	<footer class="footer-copyright text-center py-3 fixed-bottom">
		<?php include('partials/footer.html') ?>
	</footer>
	<?php include('partials/bodyRelations.html') ?>
</body>
</html>

<?php
