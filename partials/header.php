<div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
	<div class="carousel-inner">
		<?php
		$dirscan = scandir('images');
		$images = [];
		foreach ($dirscan as $img)
		{
			if (strpos($img, 'mini') === 0) {
				$images[] = $img;
			}
		}
		for ($i = 1; $i <= count($images); $i++)
		{
			$active = $i === 1 ? 'active': '';
			echo sprintf('<div class="carousel-item %s">', $active);
			echo sprintf('<img class="d-block w-100" src="images/mini_%u.jpg" alt="Mini Cooper">', $i);
			echo '</div>';
		}
		?>
	</div>
</div>
